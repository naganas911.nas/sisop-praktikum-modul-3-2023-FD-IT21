#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/msg.h>

#define MAX_SIZE 1024

struct message_buffer {
    long message_type;
    char message_text[MAX_SIZE];
};

int get_command(char* buffer){
    printf("Enter a command:\n");
    fgets(buffer, MAX_SIZE, stdin);

    if(strlen(buffer) == 0){
        printf("Error: Empty command entered.\n");
        return 0;
    }
    return 1;
}

int main() {
    key_t key;
    int msgid;
    struct message_buffer buffer;

    if(get_command(buffer.message_text) == 0){
        return 0;
    }

    key = ftok("user.c", 'A');
    msgid = msgget(key, 0666 | IPC_CREAT);

    buffer.message_type = 1;
    msgsnd(msgid, &buffer, sizeof(buffer), 0);

    return 0;
}
