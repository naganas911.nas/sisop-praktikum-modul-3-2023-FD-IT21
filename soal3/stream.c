#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/msg.h>
#include <unistd.h>

#define MAX_SIZE 1024

struct message_buffer {
    long message_type;
    char message_text[MAX_SIZE];
};

void read_input(int* choice){
    printf("Please enter your choice:\n");
    printf("1. Decrypt\n");
    printf("2. List Playlist\n");
    printf("3. Play Music\n");
    printf("4. Add Song to Playlist\n");
    printf("5. Exit\n");
    scanf("%d", choice);
}

void decrypt(){
    // code to decrypt
    printf("Decrypting...\n");
}

void list_playlist(){
    printf("Playlist:\n");
    system("cat playlist.txt");
}

void play_music(){
    // code to play music
    printf("Playing music...\n");
}

void add_to_playlist(){
    char song_name[MAX_SIZE];
    printf("Enter song name to add: ");
    scanf("%s", song_name);
    FILE *fp;
    fp = fopen("playlist.txt", "a");
    fprintf(fp, "%s\n", song_name);
    fclose(fp);
}

int main() {
    char *filename = "playlist.txt";
    if(access(filename, F_OK) == -1){
        FILE *fp;
        fp = fopen("playlist.txt", "w");
        fclose(fp);
    }

    while(1){
        int choice;
        read_input(&choice);
        switch (choice) {
            case 1:
                decrypt();
                break;
            case 2:
                list_playlist();
                break;
            case 3:
                play_music();
                break;
            case 4:
                add_to_playlist();
                break;
            case 5:
                printf("Goodbye!\n");
                return 0;
            default:
                printf("Unknown choice. Please enter a valid choice.\n");
                break;
        }
    }
    return 0;
}
