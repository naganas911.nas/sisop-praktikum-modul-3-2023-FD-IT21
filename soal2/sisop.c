#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define SHMSZ 4 * 5 * sizeof(int)

int main() {
int shmid;
key_t key = 8080;
int (*shm)[5]; // pointer ke shared memory

// membuat shared memory segment
if ((shmid = shmget(key, SHMSZ, IPC_CREAT | 0666)) < 0) {
    perror("shmget");
    exit(1);
}

// meng-attach shared memory segment 
if ((shm = shmat(shmid, NULL, 0)) == (int (*)[5]) -1) {
    perror("shmat");
    exit(1);
}

// mengambil data shared memory dan menampilkannya
printf("Hasil kali matriks:\n");
for (int i = 0; i < 4; i++) {
    for (int j = 0; j < 5; j++) {
        printf("%d ", shm[i][j]);
    }
    printf("\n");
}
printf("\n");

// menghitung dan menampilkan hasil rata-rata elemen matriks
int sum = 0;
for (int i = 0; i < 4; i++) {
    for (int j = 0; j < 5; j++) {
        sum += shm[i][j];
    }
}
double avg = (double) sum / (4 * 5);
printf("Rata-rata elemen matriks: %f\n\n", avg);

// mencari elemen terbesar dalam matriks dan menampilkannya
int max_elem = shm[0][0];
for (int i = 0; i < 4; i++) {
    for (int j = 0; j < 5; j++) {
        if (shm[i][j] > max_elem) {
            max_elem = shm[i][j];
        }
    }
}
printf("Elemen terbesar dalam matriks: %d\n\n", max_elem);

// mencari elemen terkecil dalam matriks dan menampilkannya
int min_elem = shm[0][0];
for (int i = 0; i < 4; i++) {
    for (int j = 0; j < 5; j++) {
        if (shm[i][j] < min_elem) {
            min_elem = shm[i][j];
        }
    }
}
printf("Elemen terkecil dalam matriks: %d\n\n", min_elem);

// melepaskan shared memory segment
shmdt((void *) shm);

return 0;
}
