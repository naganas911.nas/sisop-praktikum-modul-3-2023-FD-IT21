#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/shm.h>

int main()
{
    key_t key = 8080;
    int sharedMemory_id;
    typedef int int_array[5];
    int_array *sharedMemory;
    int mat1[4][2] = {{1,2}, {3,4}, {5,6}, {7,8}};
    int mat2[2][5] = {{1,1,1,1,1}, {2,2,2,2,2}};
    int result[4][5];
    int i, j, k;

    // Melakukan perkalian matriks
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            result[i][j] = 0;
            for (k = 0; k < 2; k++) {
                result[i][j] += matrix1[i][k] * matrix2[k][j];
            }
        }
    }

    // Menampilkan matriks ke layar
    printf("Matrix Result:\n");
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            printf("%d ", result[i][j]);
        }
        printf("\n");
    }

    if ((sharedMemory_id = shmget(key, 4 * 5 * sizeof(int), IPC_CREAT | 0666)) < 0) {
        perror("shmget");
        exit(1);
    }

    if ((void*)(sharedMemory = shmat(sharedMemory_id, NULL, 0)) == (void *) -1) {
        perror("shmat");
        exit(1);
    }

    memcpy(sharedMemory, result, 4 * 5 * sizeof(int));
    return 0;
}
