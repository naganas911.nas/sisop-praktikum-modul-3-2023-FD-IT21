#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <pthread.h>
#include <math.h>

#define SHMSZ 4 * 5 * sizeof(float)

// fungsi untuk menghitung pangkat dua
float power(float x) {
    return x * x;
}

// fungsi untuk menghitung pangkat dua dari setiap elemen matriks
void *fact(void *arg) {
    float *num = (float *) arg;
    float result = power(*num);
    float *res = malloc(sizeof(float));
    *res = result;
    return (void*)res;
}

int main() {
    int shmid;
    key_t key = 8080;
    float (*shm)[5];

    // mengambil shared memory segment 
    if ((shmid = shmget(key, SHMSZ, 0666)) < 0) {
        perror("shmget");
        exit(1);
    }
    // meng-attach shared memory segment
    if ((float *)(shm = shmat(shmid, NULL, 0)) == (float *) -1) {
        perror("shmat");
        exit(1);
    }

    // membuat array thread
    pthread_t threads[4 * 5];
    int thread_count = 0;

    // menampilkan matriks hasil perkalian dari program kalian.c
    printf("Matriks hasil perkalian dari program kalian.c:\n");
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            printf("%.2f ", shm[i][j]);
        }
        printf("\n");
    }
    printf("\n");
    
    //menampilkan hasil pangkat dua dari setiap elemen matriks 
    printf("Hasil pangkat dua setiap elemen matriks :\n");
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            float *num = &shm[i][j];
            float *result = malloc(sizeof(float));
            *result = power(*num);
            printf("%.2f ", *result);
            free(result);
        }
        printf("\n");
    }
    printf("\n");

    // melepaskan shared memory segment
    shmdt((void *) shm);

    return 0;
}
